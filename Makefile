COMMON_DOCKER_RUN_ARGS=--rm -it -u "$(shell id -u):$(shell id -g)"

SONARQUBE_IMAGE=sonarqube:8.6-community
SONARQUBE_CLI_IMAGE=sonarsource/sonar-scanner-cli:4.6
SONARQUBE_RUN_ARGS=$(COMMON_DOCKER_RUN_ARGS) -v "${PWD}:/usr/src:rw" -e SONAR_SCANNER_OPTS=-Xmx512m
SONARQUBE_NETWORK=sonarqube-demo

# SonarQube targets to run additional code analysis
sonarqube-start:
	docker pull $(SONARQUBE_IMAGE)
	docker network create $(SONARQUBE_NETWORK)
	docker run --name sonarqube --rm -d -p 127.0.0.1:9000:9000 --network $(SONARQUBE_NETWORK) $(SONARQUBE_IMAGE)

sonarqube-stop:
	-docker kill sonarqube
	-docker network rm $(SONARQUBE_NETWORK)

sonarqube-scan:
	docker pull $(SONARQUBE_CLI_IMAGE)
	docker run $(SONARQUBE_RUN_ARGS) --network $(SONARQUBE_NETWORK) $(SONARQUBE_CLI_IMAGE)
