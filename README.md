## SonarQube Analysis
[SonarQube](https://www.sonarqube.org) is an advanced code analysis tool used to identify security vulnerabilities, 
bugs, and bad practices. I added this to the tool set since I have found it very useful in my career. 
Fortunately, SonarSource has released images for both the scanner and server, making this basic demo/testing 
implementation quite easy.

If you wish to run the analysis yourself, run:
```shell script
make sonarqube-start
```
Then go to [http://localhost:9000](http://localhost:9000) and wait for the login page to appear.

##### Scanning
After the login page is visible, you may initiate the scan.  
When it completes, see the results here: [SonarQube Results](http://localhost:9000/dashboard?id=org%3Arepo)
```shell script
make sonarqube-scan
```

##### Shutdown SonarQube
When you are done with SonarQube, just run:
```shell script
make sonarqube-stop
```
